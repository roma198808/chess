class Piece
  attr_reader :icon, :color

  def initialize(color)
    @color = color
  end

  def take_out_check_moves(from, pos_array, board)
    valid_moves = []

    pos_array.each do |pos|

      dupped_board = board.dup
      object = dupped_board.board[from]

      dupped_board.board[pos] = object
      dupped_board.board[from] = nil

      if dupped_board.in_check?(object.color)
        next
      else
        valid_moves << pos
      end

    end

    valid_moves
  end
end

class SlidingPiece < Piece
  def pos_horizontal(pos)
    letter = pos[0]
    number = pos[1].to_i
    moves = []

    i = 1
    while i <= 8
      moves << "#{(96+i).chr}#{number}"
      i += 1
    end

    moves
  end

  def pos_vertical(pos)
    letter = pos[0]
    number = pos[1].to_i
    moves = []

    i = 1
    while i <= 8
      moves << "#{letter}#{i}"
      i += 1
    end

    moves
  end

  def pos_diagonal_up(pos, board)
    letter = pos[0]
    number = pos[1].to_i
    moves = [pos]

    i = 1

    while i <= 8
      moves << "#{(letter.ord-i).chr}#{number-i}"
      moves << "#{(letter.ord+i).chr}#{number+i}"
      i += 1
    end

    moves.sort.select { |move| board.board.keys.include?(move) }
  end

  def pos_diagonal_down(pos, board)
    letter = pos[0]
    number = pos[1].to_i
    moves = [pos]

    i = 8
    while i >= 1
      moves << "#{(letter.ord-i).chr}#{number+i}"
      moves << "#{(letter.ord+i).chr}#{number-i}"
      i -= 1
    end

    moves.sort.select { |move| board.board.keys.include?(move) }
  end

  def valid_sliding_moves(pos, sorted_array, board)
    valid_moves = []
    index = sorted_array.index(pos) # => 4

    if index == 8
      right = []
    else
      right = sorted_array[index+1..-1]
    end

    if index == 0
      left = []
    else
      left = sorted_array[0..index-1]
    end

    right.each do |pos|
      if board.board[pos].is_a?(Piece) && board.board[pos].color == @color
        break
      elsif board.board[pos].is_a?(Piece) && board.board[pos].color != @color
        valid_moves << pos
        break
      else
        valid_moves << pos
      end
    end

    left.reverse.each do |pos|
      if board.board[pos].is_a?(Piece) && board.board[pos].color == @color
        break
      elsif board.board[pos].is_a?(Piece) && board.board[pos].color != @color
        valid_moves << pos
        break
      else
        valid_moves << pos
      end
    end

    valid_moves.sort
  end
end

class SteppingPiece < Piece
end

class Rook < SlidingPiece
  def initialize(color)
    super(color)
    if color == "white"
      @icon = "\u2656"
    else
      @icon = "\u265C"
    end
  end

  def valid_moves(pos, board)
    hor_moves = pos_horizontal(pos)
    hor_valid = valid_sliding_moves(pos, hor_moves, board)

    ver_moves = pos_vertical(pos)
    ver_valid = valid_sliding_moves(pos, ver_moves, board)

    hor_valid + ver_valid
  end
end

class Knight < SteppingPiece
  def initialize(color)
    super(color)
    if color == "white"
      @icon = "\u2658"
    else
      @icon = "\u265E"
    end
  end

  def valid_moves(pos, board)
     all_possible_moves = pos_knight_moves(pos, board)

     semi_valid_moves = []

     all_possible_moves.each do |move|
       if board.board[move].is_a?(Piece) && board.board[move].color == @color
         next
       else
         semi_valid_moves << move
       end
     end

     semi_valid_moves
  end

  def pos_knight_moves(pos, board)
    valid_moves = []

    valid_moves << "#{(pos[0].ord-1).chr}#{pos[1].to_i+2}"
    valid_moves << "#{(pos[0].ord+1).chr}#{pos[1].to_i+2}"
    valid_moves << "#{(pos[0].ord+2).chr}#{pos[1].to_i+1}"
    valid_moves << "#{(pos[0].ord+2).chr}#{pos[1].to_i-2}"
    valid_moves << "#{(pos[0].ord+1).chr}#{pos[1].to_i-2}"
    valid_moves << "#{(pos[0].ord-1).chr}#{pos[1].to_i-2}"
    valid_moves << "#{(pos[0].ord-2).chr}#{pos[1].to_i-1}"
    valid_moves << "#{(pos[0].ord-2).chr}#{pos[1].to_i+1}"

     valid_moves.select { |move| board.board.keys.include?(move) }
  end
end

class Bishop < SlidingPiece
  def initialize(color)
    super(color)
    if color == "white"
      @icon = "\u2659"
    else
      @icon = "\u265D"
    end
  end

  def valid_moves(pos, board)
    diag_up_moves = pos_diagonal_up(pos,board)
    diag_up_valid = valid_sliding_moves(pos, diag_up_moves, board)

    diag_down_moves = pos_diagonal_down(pos,board)
    diag_down_valid = valid_sliding_moves(pos, diag_down_moves, board)

    diag_down_valid + diag_up_valid
  end
end

class Queen < SlidingPiece
  def initialize(color)
    super(color)
    if color == "white"
      @icon = "\u2655"
    else
      @icon = "\u265B"
    end
  end

  def valid_moves(pos, board)
    diag_up_moves = pos_diagonal_up(pos,board)
    diag_up_valid = valid_sliding_moves(pos, diag_up_moves, board)

    diag_down_moves = pos_diagonal_down(pos,board)
    diag_down_valid = valid_sliding_moves(pos, diag_down_moves, board)

    hor_moves = pos_horizontal(pos)
    hor_valid = valid_sliding_moves(pos, hor_moves, board)

    ver_moves = pos_vertical(pos)
    ver_valid = valid_sliding_moves(pos, ver_moves, board)

    diag_down_valid + diag_up_valid + hor_valid + ver_valid
  end

end

class King < SteppingPiece
  def initialize(color)
    super(color)
    if color == "white"
      @icon = "\u2654"
    else
      @icon = "\u265A"
    end
  end

  def valid_moves(pos, board)
    all_possible_moves = pos_king_moves(pos, board)

    semi_valid_moves = []

    all_possible_moves.each do |move|
      if board.board[move].is_a?(Piece) && board.board[move].color == @color
        next
      else
        semi_valid_moves << move
      end
    end

    semi_valid_moves
  end

  def pos_king_moves(pos, board)
    valid_moves = []

    valid_moves << "#{(pos[0].ord-1).chr}#{pos[1].to_i+1}"
    valid_moves << "#{(pos[0].ord+0).chr}#{pos[1].to_i+1}"
    valid_moves << "#{(pos[0].ord+1).chr}#{pos[1].to_i+1}"
    valid_moves << "#{(pos[0].ord+1).chr}#{pos[1].to_i+0}"
    valid_moves << "#{(pos[0].ord+1).chr}#{pos[1].to_i-1}"
    valid_moves << "#{(pos[0].ord+0).chr}#{pos[1].to_i-1}"
    valid_moves << "#{(pos[0].ord-1).chr}#{pos[1].to_i-1}"
    valid_moves << "#{(pos[0].ord-1).chr}#{pos[1].to_i+0}"

    valid_moves.select { |move| board.board.keys.include?(move) }
  end
end

class Pawn < Piece
  def initialize(color, initial_pos)
    super(color)
    if color == "white"
      @icon = "\u2659"
    else
      @icon = "\u265F"
    end
    @initial_pos = initial_pos
  end

  def valid_moves(pos, board)
    valid_moves = []

    if @initial_pos[1] == "2"
      valid_moves += valid_up_moves(pos, board)
    else
      valid_moves += valid_down_moves(pos, board)
    end

    valid_moves.select { |move| board.board.keys.include?(move) }
  end

  def valid_up_moves(pos, board)
    valid_moves = []

    if pos == @initial_pos
      unless board.board["#{pos[0]}#{pos[1].to_i+2}"].is_a?(Piece) ||
             board.board["#{pos[0]}#{pos[1].to_i+1}"].is_a?(Piece)

             valid_moves << "#{pos[0]}#{pos[1].to_i+2}"
      end
    end

    unless board.board["#{pos[0]}#{pos[1].to_i+1}"].is_a?(Piece)
      valid_moves << "#{pos[0]}#{pos[1].to_i+1}"
    end

    if board.board["#{(pos[0].ord-1).chr}#{pos[1].to_i+1}"].is_a?(Piece) &&
       board.board["#{(pos[0].ord-1).chr}#{pos[1].to_i+1}"].color != @color

       valid_moves << "#{(pos[0].ord-1).chr}#{pos[1].to_i+1}"
    end

    if board.board["#{(pos[0].ord+1).chr}#{pos[1].to_i+1}"].is_a?(Piece) &&
       board.board["#{(pos[0].ord+1).chr}#{pos[1].to_i+1}"].color != @color

       valid_moves << "#{(pos[0].ord+1).chr}#{pos[1].to_i+1}"
    end

    valid_moves
  end

  def valid_down_moves(pos, board)
    valid_moves = []

    if pos == @initial_pos
      unless board.board["#{pos[0]}#{pos[1].to_i-2}"].is_a?(Piece) ||
             board.board["#{pos[0]}#{pos[1].to_i-1}"].is_a?(Piece)

             valid_moves << "#{pos[0]}#{pos[1].to_i-2}"
      end
    end

    unless board.board["#{pos[0]}#{pos[1].to_i-1}"].is_a?(Piece)
      valid_moves << "#{pos[0]}#{pos[1].to_i-1}"
    end

    if board.board["#{(pos[0].ord-1).chr}#{pos[1].to_i-1}"].is_a?(Piece) &&
       board.board["#{(pos[0].ord-1).chr}#{pos[1].to_i-1}"].color != @color

       valid_moves << "#{(pos[0].ord-1).chr}#{pos[1].to_i-1}"
    end

    if board.board["#{(pos[0].ord+1).chr}#{pos[1].to_i-1}"].is_a?(Piece) &&
       board.board["#{(pos[0].ord+1).chr}#{pos[1].to_i-1}"].color != @color

       valid_moves << "#{(pos[0].ord+1).chr}#{pos[1].to_i-1}"
    end

    valid_moves
  end
end