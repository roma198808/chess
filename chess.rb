require_relative 'piece'
require_relative 'board'
require 'colorize'

# Stll, Beautiful code!

class Chess
  def initialize
    @board = Board.new
    @color = "white"
  end

  def run
    until @board.checkmate?(@color)
      @board.display
      puts "You're in check." if @board.in_check?(@color)
      get_move
      switch_color
    end

    puts "Checkmate!"
  end

  private

  def get_move
    puts "#{@color.capitalize}:"
    valid_move = false
    until valid_move
      from = get_start_position
      to   = get_end_position

      valid_move = @board.move(from, to)
      puts "Invalid Move" unless valid_move
    end
  end

  def get_start_position
    from = nil
    while from.nil?
      puts "Move from:"
      from = gets.chomp
      from = from.scan(/[a-h][1-8]/).first
      if @board.board[from].nil? || @board.board[from].color != @color
        from = nil
      end
    end
    from
  end

  def get_end_position
    to = nil
    while to.nil?
      puts "Move to:"
      to = gets.chomp
      to = to.scan(/[a-h][1-8]/).first
    end
    to
  end

  def switch_color
    @color == "white" ? @color = "black" : @color = "white"
  end

end

c = Chess.new
c.run