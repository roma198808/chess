class Board
  attr_accessor :board

  def initialize
    @board = create_initial_board
  end

  def move(from, to)
    piece_object = @board[from]

    semi_valid_moves = piece_object.valid_moves(from, self)
    valid_moves = piece_object.take_out_check_moves(from, semi_valid_moves, self)

    if valid_moves.include?(to)
      @board[to]   = piece_object
      @board[from] = nil
      true
    else
      false
    end
  end

  def display
    colorize_row = "odd"

     (1..8).to_a.reverse.each do |index|
      print "#{index} "

      colorize_row == "odd" ? colorize_cell = "odd" : colorize_cell = "even"
      ('a'..'h').to_a.each do |letter|

        if @board[letter + index.to_s] == nil
          if colorize_cell == "odd"
            print "   ".colorize(:background => :red)
          else
            print "   "
          end
        else
          if colorize_cell == "odd"
            print " #{@board[letter + index.to_s].icon} ".colorize(:background => :red)
          else
            print " #{@board[letter + index.to_s].icon} "
          end
        end

        colorize_cell == "odd" ? colorize_cell = "even" : colorize_cell = "odd"
      end
      colorize_row == "odd" ? colorize_row = "even" : colorize_row = "odd"
      puts
    end
    print "   a  b  c  d  e  f  g  h"
    puts
  end

  def checkmate?(color)
    all_pieces = all_pieces(color)

    all_pieces.each do |pos, piece|
      semi_valid_moves = piece.valid_moves(pos, self)
      valid_moves = piece.take_out_check_moves(pos, semi_valid_moves, self)
      return false if valid_moves.count > 0
    end
    true
  end

  def in_check?(color)
    king = @board.values.select { |piece| piece.class == King &&
                                          piece.color == color}[0]
    kings_pos = @board.key(king)

    opponent_pieces_hash = @board.select { |pos, object| object != nil &&
                                                         object.color != color }
    all_valid_moves = []

    opponent_pieces_hash.each do |pos, object|
      all_valid_moves += object.valid_moves(pos, self)
    end

    all_valid_moves.any? { |move| move == kings_pos }
  end

  def dup
    object = Board.new
    self.board.each do |key,value|
      object.board[key] = value
    end
    object
  end

  private

  def empty_board
    hash = {}
    ('a'..'h').each do |letter|
      x = 1
      8.times do
        hash[letter + x.to_s] = nil
        x += 1
      end
    end
    hash
  end

  def create_initial_board
    board = empty_board

    board["a1"] = Rook.new("white")
    board["b1"] = Knight.new("white")
    board["c1"] = Bishop.new("white")
    board["d1"] = Queen.new("white")
    board["e1"] = King.new("white")
    board["f1"] = Bishop.new("white")
    board["g1"] = Knight.new("white")
    board["h1"] = Rook.new("white")
    board["a2"] = Pawn.new("white","a2")
    board["b2"] = Pawn.new("white","b2")
    board["c2"] = Pawn.new("white","c2")
    board["d2"] = Pawn.new("white","d2")
    board["e2"] = Pawn.new("white","e2")
    board["f2"] = Pawn.new("white","f2")
    board["g2"] = Pawn.new("white","g2")
    board["h2"] = Pawn.new("white","h2")

    board["a8"] = Rook.new("black")
    board["b8"] = Knight.new("black")
    board["c8"] = Bishop.new("black")
    board["d8"] = Queen.new("black")
    board["e8"] = King.new("black")
    board["f8"] = Bishop.new("black")
    board["g8"] = Knight.new("black")
    board["h8"] = Rook.new("black")
    board["a7"] = Pawn.new("black","a7")
    board["b7"] = Pawn.new("black","b7")
    board["c7"] = Pawn.new("black","c7")
    board["d7"] = Pawn.new("black","d7")
    board["e7"] = Pawn.new("black","e7")
    board["f7"] = Pawn.new("black","f7")
    board["g7"] = Pawn.new("black","g7")
    board["h7"] = Pawn.new("black","h7")

    board
  end

  def all_pieces(color)
    @board.select do |pos, piece|
      next if piece.nil?
      piece.color == color
    end
  end

end
